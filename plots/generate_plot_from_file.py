from plots.csv_file_integration import CSVFile
from plots.visualisation import *

file: CSVFile = CSVFile.create("../benchmarks/data_multi_1000_600.csv", dimensions=1)

key, value = file.read_data()

parsed_value: Dict[float, Tuple[float, bool]] = {}

for iterator in range(len(key)):
    parsed_value[key[iterator]] = (value[iterator][0], False)

visualize_scatter_dots(parsed_value, "Time", "Response Time", "Response Time over Time")