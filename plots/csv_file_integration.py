import csv
from typing import List, Tuple


class CSVFile:
    reader: csv.reader = None
    writer: csv.writer = None

    path: str = ""
    dimensions: int = 2

    @staticmethod
    def create(path: str, dimensions: int = 2) -> 'CSVFile':

        file: CSVFile = CSVFile()
        file.path = path
        file.dimensions = dimensions

        return file

    def write_data(self, key: List[float], data: List[List[float]]):
        if len(data[0]) != self.dimensions:
            raise ValueError("Expected Array with " + str(self.dimensions) + " of entries")
        with open(self.path, "a") as file:
            writer = csv.writer(file, delimiter="/", quotechar="#")

            for index, key in enumerate(key):
                writer.writerow([key] + data[index])

    def read_data(self) -> Tuple[List[float], List[List[float]]]:

        key: List[float] = []
        data: List[List[float]] = []

        with open(self.path) as file:
            reader = csv.reader(file, delimiter="/", quotechar="#")

            for row in reader:
                row = [float(x) for x in row]
                key.append(row[0])
                data.append(row[1:])

        return key, data
