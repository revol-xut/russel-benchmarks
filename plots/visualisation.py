import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
from typing import Dict, Tuple, List


def process_data(data: Dict[float, Tuple[float, bool]]) -> Tuple[List[float], List[float]]:
    x_data_1: List[float] = []
    y_data_1: List[float] = []

    for key, value in data.items():
        x_data_1.append(key)
        y_data_1.append(value[0])
    return x_data_1, y_data_1


def workload_over_time(data: Dict[float, Tuple[float, bool]], size: int, time_intervals: float = 0.1):
    newly_generated: List[int] = [0] * int(max(list(data.keys())) / time_intervals)

    for key, value in data.items():
        newly_generated[int(key / time_intervals)] += 1


def visualise(data: Dict[float, Tuple[float, bool]], x_axis: str, y_axis: str, title: str):
    plt.title(title)
    plt.ylabel(y_axis)
    plt.xlabel(x_axis)
    x_data_1, y_data_1 = process_data(data)
    plt.plot(x_data_1, y_data_1, ".-")

    plt.show()


def visualize_scatter_dots(data: Dict[float, Tuple[float, bool]], x_axis: str, y_axis: str, title: str):
    x_data_1, y_data_1 = process_data(data)

    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x_data_1, y_data_1)
    print(slope, intercept, r_value, p_value, std_err)

    plt.plot(x_data_1, y_data_1, "ro")
    plt.plot([x_data_1[0], x_data_1[-1]], [slope * x_data_1[0] + intercept, slope * x_data_1[-1] + intercept])

    plt.xlabel(x_axis, fontsize=15)
    plt.ylabel(y_axis, fontsize=15)
    plt.title("Regression")

    plt.grid(True)
    plt.tight_layout()

    plt.show()
