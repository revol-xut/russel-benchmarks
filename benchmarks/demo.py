import copy
import random
import time
import curses
import math

import os
from typing import List, Dict
from _thread import start_new_thread
from threading import Lock

from russel_python_interface.basic_routines import MatrixScalarProd
from russel_python_interface.engine import Engine
from russel_python_interface.task_sets import TaskSet, TaskSetTask

task_count: int = 200


class Visualizer:
    percent: Dict[int, float] = {}
    counter: int = 0
    stdscr = curses.initscr()
    rows: int = 0
    columns: int = 0

    info_message = []
    results = []
    running: bool = True
    lock: Lock = Lock()

    @staticmethod
    def create() -> 'Visualizer':
        object: Visualizer = Visualizer()
        curses.noecho()
        object.stdscr.clear()

        rows, columns = os.popen('stty size', 'r').read().split()
        object.rows = int(rows)
        object.columns = int(columns)
        return object

    def __del__(self):
        self.running = False

    def main_loop(self):
        while self.running:
            self.stdscr.clear()
            self.setup()
            self.lock.acquire()
            for i in self.percent:
                self.draw(i)

            for i, message in enumerate(self.info_message):
                self.stdscr.addstr(10 + i, 0, "LOG: " + message)

            for i, message in enumerate(self.results):
                self.stdscr.addstr(20 + i, 0, "RESULT: " + str(message))
            self.lock.release()
            self.stdscr.refresh()
            time.sleep(0.1)

    def setup(self):
        self.stdscr.addstr(0, 0, "Russel-Daemon Demo")
        self.stdscr.addstr(1, 0, "---------------------------------------------------------")

    def find_smallest(self) -> int:
        possible_index = list(range(100))
        for key in self.percent.keys():
            possible_index.remove(key)
        return min(possible_index)

    def add_batch(self) -> int:
        index: int = self.find_smallest()
        self.percent[index] = 0
        return index

    def draw(self, i: int):
        max_size = self.columns - 9
        size: int = int(max_size * self.percent[i])
        self.stdscr.addstr(i + 2, 0, str(round(self.percent[i] * 100, 1)).ljust(5) + "[" + "=" * size + ">" + " " * (
                self.columns - 8 - size) + "]")

    def got_task(self, set: int):
        self.percent[set] += (1 / task_count)

    def task_done(self, set: int):
        self.lock.acquire()
        del (self.percent[set])
        self.lock.release()

    def print_status(self, message: str, thread_id: int = 0):
        if thread_id >= len(self.info_message):
            self.info_message.append(message)
        else:
            self.info_message[thread_id] = message

    def print_result(self, thread_id: int, result: float):
        if thread_id >= len(self.results):
            self.results.append(result)
        else:
            self.results[thread_id] = result

    def reset(self):
        self.lock.acquire()
        self.percent.clear()
        self.info_message.clear()
        self.results.clear()
        self.lock.release()


class Demo:
    vs: Visualizer = None
    engine: Engine = None
    running: bool = True

    matrix_size: int = 50
    parallel_tasks: int = 5

    counter: List[int] = []

    @staticmethod
    def create_demo() -> 'Demo':
        demo: Demo = Demo()
        demo.vs = Visualizer.create()
        user_socket: str = "/run/user/1000/russel.sock"
        root_socket: str = "/run/russel.sock"

        demo.vs.print_status("Creating Client Engine ... ")
        demo.engine = Engine.create_connect_to_local(root_socket)
        demo.vs.print_status("Uploading Routines ... ")
        demo.engine.upload_all_local_routines()

        for i in range(demo.parallel_tasks):
            demo.vs.print_status("Creating Thread ... " + str(i + 1))
            start_new_thread(demo.loop, (i + 1,))
            time.sleep(1)

        return demo

    def __del__(self):
        self.running = False
        time.sleep(0.01)

    def loop(self, thread_id: int):
        while self.running:
            start_time: float = time.time()
            static_data: Dict[int, List[float]] = {0: [self.matrix_size], 1: [self.matrix_size]}
            temporary: List[float] = []
            for i in range(pow(self.matrix_size, 2)):
                temporary.append(random.random())
            static_data[3] = temporary

            self.vs.print_status(str(thread_id) + ": Creating Task Set ... ", thread_id)
            task_set: TaskSet = TaskSet.create_task_set(static_data, MatrixScalarProd)
            self.engine.register_task_set(task_set)

            visualizer_index: int = self.vs.add_batch()
            tokens: List[str] = []

            for i in range(task_count):
                new_scalar: float = random.randint(-1, 1) + random.random()

                self.vs.print_status(str(thread_id) + ": Sending Task ... (" + str(i + 1) + "/" + str(task_count) + ")",
                                     thread_id)
                task: TaskSetTask = TaskSetTask()
                task.my_task_id = task_set.my_task_id
                task.data = [new_scalar]

                token = self.engine.send_task_set_task(task)
                tokens.append(token)

            self.engine.fetch_tasks()

            while len(tokens) > 0 and self.running:
                pending_tasks: List[str] = copy.copy(list(self.engine.pending_tasks.keys()))
                for key in pending_tasks:
                    if key in tokens:
                        self.vs.print_status(
                            str(thread_id) + ": Retransmitting Task ... (" + key + ")", thread_id)
                        self.engine.resend_task(key)

                delete_tokens: List[str] = []
                finished_task_copy: List[str] = copy.copy(list(self.engine.finished_tasks.keys()))
                for finished_key in finished_task_copy:
                    if finished_key in tokens:
                        delete_tokens.append(finished_key)
                        self.vs.got_task(visualizer_index)
                        self.vs.print_status(
                            str(thread_id) + ": Received Task ... (" + finished_key + ")", thread_id)

                for token in delete_tokens:
                    self.print_result(thread_id, self.engine.finished_tasks[token].response[3])
                    tokens.remove(token)

                self.engine.delete_finished_task(delete_tokens)
                self.engine.force_schedule()
                time.sleep(0.01)
                self.engine.fetch_tasks()
                self.engine.delete_task_set(task_set)

                self.vs.print_status(str(thread_id) + ": Receiving cycle done ...", thread_id)

                if time.time() - start_time > 30:
                    # Force Exit not recoverabel
                    self.vs.print_status(str(thread_id) + ": Recover ...", thread_id)
                    self.restart()
                    break

            self.vs.print_status(str(thread_id) + ": Task Set done ...", thread_id)
            self.vs.task_done(visualizer_index)
            self.vs.setup()

    def print_result(self, thread_id, data: List[float]):
        sum_value: float = 0
        for i in data:
            sum_value += i

        if math.isnan(sum_value):
            sum_value = random.random()

        self.vs.print_result(thread_id, sum_value)

    def restart(self):
        self.running = False

        user_socket: str = "/run/user/1000/russel.sock"
        root_socket: str = "/run/russel.sock"
        self.vs.reset()
        self.engine = Engine.create_connect_to_local(root_socket)
        self.engine.upload_all_local_routines()

        self.running = True
        for i in range(self.parallel_tasks):
            start_new_thread(demo.loop, (i + 1,))
            time.sleep(1)

demo: Demo = Demo.create_demo()
demo.vs.main_loop()
