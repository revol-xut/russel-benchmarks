import time
from typing import List

from russel_python_interface.basic_routines import MatrixScalarProd
from russel_python_interface.engine import Engine
from russel_python_interface.methods_file import create_random_task, calculate_difference, average_error, expected_return_value
from russel_python_interface.task import Task

#######################################
# Config
matrix_size: int = 10

my_engine: Engine = Engine.create_connect_to_local("/run/user/1000/russel.sock")
my_engine.upload_all_local_routines()

my_engine.start()

data: List[float] = create_random_task(matrix_size)

token: str = my_engine.run_template_task(MatrixScalarProd(), data)
time_1: float = time.time()
my_engine.force_schedule()

print("waiting ... for uuid: " + token)
while not my_engine.task_done(token):
    time.sleep(0.01)
    # print("sleep")

task: Task = my_engine.get_task(token)

time_2: float = time.time()

difference: List[float] = calculate_difference(task.response[3], expected_return_value(data))

print("Error Matrix:")

# proper_print(difference, matrix_size)
# task.
print("Average error:", average_error(difference))
print("Needed Time:", time_2 - time_1)
