import random
import time
import collections
from typing import Dict, List, Tuple

from russel_python_interface.basic_routines import MatrixScalarProd
from russel_python_interface.multi_engine_network import MultiEngineNetwork
from russel_python_interface.task_sets import TaskSet, TaskSetTask
from plots.visualisation import visualize_scatter_dots,process_data
from plots.csv_file_integration import CSVFile

task_count: int = 1000
matrix_size: int = 600
time_wait_between_tasks: float = 1

endpoints = [["/run/russel.sock", "IPC"], ["192.168.54.54:8321", "PUB"], ["192.168.54.51:8321", "PUB"]]
network: MultiEngineNetwork = MultiEngineNetwork.create(endpoints, [6, 2, 5])

static_data: Dict[int, List[float]] = {0: [matrix_size], 1: [matrix_size]}
temporary: List[float] = []
for i in range(pow(matrix_size, 2)):
    temporary.append(random.random())
static_data[3] = temporary

task_set: TaskSet = TaskSet.create_task_set(static_data, MatrixScalarProd)
network.send_task_set(task_set)

tasks: List[TaskSetTask] = []

start_time: float = time.time()

for i in range(task_count):
    new_scalar: float = random.random()

    task: TaskSetTask = TaskSetTask()
    task.my_task_id = task_set.my_task_id
    task.data = [new_scalar]

    tasks.append(task)

    if len(tasks) >= 50:
        network.solve_task_batch(tasks)
        tasks = []
        print(i)
        time.sleep(time_wait_between_tasks)

network.solve_task_batch(tasks)
network.wait()

end_time: float = time.time()

network.delete_task_set(task_set)

merged_data: Dict[float, Tuple[float, bool]] = {}

for eng in network.persistent:
    t = eng.benchmark_data.response_times.copy()
    merged_data.update(t)

print("Time Needed:", end_time - start_time)
print("Operations performed: ", (matrix_size * matrix_size * task_count))
print("Operations per Second:", (matrix_size * matrix_size * task_count) / (end_time - start_time))

od = collections.OrderedDict(sorted(merged_data.items()))

d1, d2 = process_data(od)

data_sink: CSVFile = CSVFile.create("data_multi_" + str(task_count) + "_" + str(matrix_size) + ".csv", dimensions=1)

new_d2 = [[x] for x in d2]

for index in range(len(d1)):
    data_sink.write_data(d1, new_d2)