import copy
import random
import time
from typing import List, Dict

from russel_python_interface.basic_routines import MatrixScalarProd
from russel_python_interface.engine import Engine
from russel_python_interface.task import Task
from russel_python_interface.task_sets import TaskSet, TaskSetTask
from plots.csv_file_integration import CSVFile
from plots import visualisation

task_count: int = 250
matrix_size: int = 300
time_wait_between_tasks: float = 0.02

user_socket: str = "/run/user/1000/russel.sock"
root_socket: str = "/run/russel.sock"
#my_engine: Engine = Engine.create_connect_to_local(root_socket, benchmark=True)
my_engine: Engine = Engine.create_connect_to_network("192.168.54.54", 8321, benchmark=True)
my_engine.upload_all_local_routines()
my_engine.start()

flop_counter: int = 0
undone_task_counter: int = 0
benchmark_latency: float = 0
start_send_task: float = 0

expecting_task: Dict[str, float] = {}

static_data: Dict[int, List[float]] = {0: [matrix_size], 1: [matrix_size]}
temporary: List[float] = []
for i in range(pow(matrix_size, 2)):
    temporary.append(random.random())
static_data[3] = temporary

task_set: TaskSet = TaskSet.create_task_set(static_data, MatrixScalarProd)
my_engine.register_task_set(task_set)


def task_respond_handler(token: str, task: Task):
    global flop_counter, undone_task_counter, expecting_task
    if token in expecting_task:
        del expecting_task[token]
        undone_task_counter -= 1

    for key in task.response:
        flop_counter += len(task.response[key])


my_engine.set_task_handler(task_respond_handler)

start_time: float = time.time()
for i in range(task_count):
    start_send_task = time.time()
    new_scalar: float = random.random()

    task: TaskSetTask = TaskSetTask()
    task.my_task_id = task_set.my_task_id
    task.data = [new_scalar]

    token = my_engine.send_task_set_task(task)
    expecting_task[token] = time.time()
    undone_task_counter += 1
    benchmark_latency += time.time() - start_send_task
    time.sleep(time_wait_between_tasks)

    if i % 10 == 0:
        print(i)

my_engine.force_schedule()

while len(expecting_task) > 0:

    temp_tokens = copy.deepcopy(expecting_task)

    for task_token in temp_tokens:
        my_engine.resend_task(task_token)
        time.sleep(time_wait_between_tasks)

    my_engine.force_schedule()
    print("Length", len(expecting_task), undone_task_counter)
    time.sleep(0.001)

end_time: float = time.time()
my_engine.delete_task_set(task_set)

my_engine.kill()

print("\n" * 3)
print("========================== Benchmark Results ==========================")

my_engine.benchmark_data.finish_benchmark_response_time()
print(my_engine.benchmark_data)

print("\n========================== External Results ==========================")

print("Time needed:", end_time - start_time)
print("Operations Performed: ", flop_counter)
print("Operations per second: ", round(flop_counter / (end_time - start_time), 2))
print("Benchmark Latency: ", benchmark_latency / task_count)

print("\n========================== Configuration ==========================")
print("Matrix Size: ", matrix_size)
print("Task Count: ", task_count)
print("Artificial time delay between task send: ", time_wait_between_tasks)

d1, d2 = visualisation.process_data(my_engine.benchmark_data.response_times)

data_sink: CSVFile = CSVFile.create("data_single_"+ str(task_count) + "_" + str(matrix_size) + ".csv", dimensions=1)

new_d2 = [[x] for x in d2]

for index in range(len(d1)):
    data_sink.write_data(d1, new_d2)