Russel - Benchmarks
----------------------

**Contact**: <revol-xut@protonmail.com>


This Repository contains various benchmark and testing scripts of the russel projects furthermore also some script for processing 
of the generated data e.g. generating plots or saving data in csv files.

## Dependencies 

All packages are available over pip.

  - russel_python_interface
  - scipy
  - numpy
  - matplotlib

## Various Scripts

 - **automated_overhead_measurement**: Creates multiple TaskSets and increments the task size and measures how fast the engine is solving the new task => Floating Points per Second from Task size 
 - **multi_engine_example**: Connect to multiple engines and test all of them 
 - **simple_task**: Connects to engine send one tasks and measures accuracy of the calculations
 - **speed_test**: Does not use the tasks set feature
 - **task_set_test**: Uses and tests the Task Set feature
